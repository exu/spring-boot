package cn.xeyuan.ws.jsr;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * @author exu
 * @version 1.0
 * @date 2017/7/19 11:51
 * 使用springboot内置tomcat进行部署的话，在编写websocket具体实现类之前，要注入ServerEndpointExporter，这个bean会自动注册使用了@ServerEndpoint注解声明的Websocket endpoint。
 * 而如果使用war包部署到tomcat中进行部署的话，就不必做此步骤，因为它将由容器自己提供和管理。
 */
@EnableAutoConfiguration
public class WebSocketConfig {
    @Bean
    public MyWebSocket serverEndpoint() {
        return new MyWebSocket();
    }
}
