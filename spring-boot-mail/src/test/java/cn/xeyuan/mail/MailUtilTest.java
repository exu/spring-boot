package cn.xeyuan.mail;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.*;

/**
 * @author exu
 * @version 1.0
 * @date 2017/8/18 17:25
 */
public class MailUtilTest extends SpringBootMailApplicationTests {
    @Autowired
    private MailUtil mailUtil;
    String to = "461791097@qq.com";
    @Test
    public void sendSimpleMail() throws Exception {
        mailUtil.sendSimpleMail(to,"简单邮件","简单邮件内容");
    }

    @Test
    public void sendHtmlMail() throws Exception {
        String content="<html>\n" +
                "<body>\n" +
                "    <h3>hello world ! 这是一封Html邮件!</h3>\n" +
                "</body>\n" +
                "</html>";
        mailUtil.sendHtmlMail(to,"html邮件",content);
    }

    @Test
    public void sendAttachmentsMail() throws Exception {
        // 获取项目路径
        File directory = new File("");// 参数为空
        String courseFile = directory.getCanonicalPath();
        String filePath = courseFile + "\\src\\main\\resources\\static\\image\\abc.jpg";
        System.out.println("附件路径="+filePath);
        mailUtil.sendAttachmentsMail(to, "主题：带附件的邮件", "有附件，请查收！", filePath);
    }
    //嵌入静态资源的邮件已经发送
    @Test
    public void sendInlineResourceMail() throws Exception {
        String rscId = "neo006";
        String content="<html><body>这是有图片的邮件：<img src=\'cid:" + rscId + "\' ></body></html>";
        File directory = new File("");// 参数为空
        String courseFile = directory.getCanonicalPath();
        String imgPath = courseFile + "\\src\\main\\resources\\static\\image\\abc.jpg";
        mailUtil.sendInlineResourceMail(to, "主题：这是有图片的邮件", content, imgPath, rscId);
    }

    @Test
    public void sendTemplateMail() throws Exception {
        //创建邮件正文
        mailUtil.sendTemplateMail(to,"主题：这是模板邮件","emailTemplate");
    }

}