package cn.xeyuan.contrller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author exu
 * @version 1.0
 * @date 2017/7/28 15:26
 */
@Controller
public class HelloWorldController {
    @RequestMapping("/index")
    public String index() {
        return "index";
    }
}
