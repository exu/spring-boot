package cn.xeyuan.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * 之定义过滤器
 * @author exu
 * @version 1.0
 * @date 2017/8/14 11:51
 */
public class IndexFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("进入filter");
        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
