package cn.xeyuan.fastjson;

import org.apache.catalina.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * user controller
 * @author exu
 * @version 1.0
 * @date 2017/8/10 16:32
 */
@RestController()
@RequestMapping("/user")
public class UserController {
    /**
     * 用户列表
     * @return
     * @authr exu
     * @date 2017/8/10 16:37
     */
    @RequestMapping("/list")
    public List<User> list() {
        List<User> userList = new ArrayList<>();
        User user = new User();
        user.setName("愚公移码不移山");
        user.setUserId(1L);
        user.setAge(null);
        user.setCreateDate(new Date());
        userList.add(user);
        return userList;
    }
}
