package cn.xeyuan.fastjson;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * user 实体类
 * @author exu
 * @version 1.0
 * @date 2017/8/10 16:31
 */
public class User implements java.io.Serializable{
    private Long userId;
    private String name;
    private Long age;
    private Date createDate;
    public User() {

    }

    @JSONField(serialize=false)
    public Long getUserId() {
        return userId;
    }
    public String getEnUserId() {
        return "加密后："+userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
