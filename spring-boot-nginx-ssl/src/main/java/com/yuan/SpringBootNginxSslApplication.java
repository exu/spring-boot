package com.yuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootNginxSslApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootNginxSslApplication.class, args);
	}
}
