package com.yuan;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试界面
 * @author exu
 * @version 1.0
 * @date 2017/7/28 17:07
 */
@RestController
@RequestMapping("/hello")
public class HelloController {
    @RequestMapping("/{name}")
    public String hello(@PathVariable("name") String name) {
        return "hello " + name;
    }
}
