package cn.xeyuan;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootLog4jApplication {
 	static Logger logger = LoggerFactory.getLogger(SpringBootLog4jApplication.class);
 	static Logger loggerFile = LoggerFactory.getLogger("logFile");
	public static void main(String[] args) {
		SpringApplication.run(SpringBootLog4jApplication.class, args);
		logger.debug("打印到控制台-debug级别");
		logger.info("打印到控制台-info级别");
		loggerFile.info("打印到输出文件");
		loggerFile.debug("打印到输出文件--debug");
	}
}
