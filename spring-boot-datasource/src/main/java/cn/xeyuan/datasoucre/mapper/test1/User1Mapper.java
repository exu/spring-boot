package cn.xeyuan.datasoucre.mapper.test1;

import cn.xeyuan.datasoucre.enitiy.User;

import java.util.List;

/**
 * user mapper
 * @author exu
 * @version 1.0
 * @date 2017/8/14 10:31
 */
public interface User1Mapper {
    List<User> getAll();

    User getOne(Long id);

    void insert(User user);

    void update(User user);

    void delete(Long id);
}
