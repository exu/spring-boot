package cn.xeyuan.datasoucre.enums;

/**
 * 用户性别枚举
 * @author exu
 * @version 1.0
 * @date 2017/8/14 10:28
 */
public enum UserSexEnum {
    MAN("man","男"),
    WOMAN("woman","女");
    private String key;
    private String title;

    UserSexEnum(String key, String title) {
        this.key = key;
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
