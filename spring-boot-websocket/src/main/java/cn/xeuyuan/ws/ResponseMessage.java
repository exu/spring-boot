package cn.xeuyuan.ws;

/**
 * @author exu
 * @version 1.0
 * @date 2017/7/18 16:11
 */
public class ResponseMessage {
    private String responseMessage;

    public ResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
