package cn.xeuyuan.ws;

/**
 * @author exu
 * @version 1.0
 * @date 2017/8/4 10:49
 */
public class Constants {
    private Constants(){}

    /**
     * SessionId
     */
    public static final String SESSIONID = "sessionid";

    /**
     * Session对象Key, 用户id
     */
    public static final String SKEY_ACCOUNT_ID = "accountId";

}
