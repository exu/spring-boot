package cn.xeuyuan.ws;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存常量名称
 * Created by earl on 2017/4/17.
 */
public class CacheConstant {

    private CacheConstant(){}

    /**
     * websocket用户accountId
     */
    public static final String WEBSOCKET_ACCOUNT = "websocket_account";
    public static Map<String,String> map = new ConcurrentHashMap<>();
}