package cn.xeuyuan.ws;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.socket.config.WebSocketMessageBrokerStats;

/**
 * @author exu
 * @version 1.0
 * @date 2017/7/18 16:11
 */
@Controller
public class WsController {
    @MessageMapping("/welcome")
    //注解表示当服务器有消息需要推送的时候，会对订阅了@SendTo中路径的浏览器发送消息
    @SendTo("/topic/getResponse")
    public ResponseMessage say(RequestMessage message) {
        System.out.println(message.getName());
        return new ResponseMessage("welcome," + message.getName() + " !");
    }
    @RequestMapping("/getWs")
    @ResponseBody
    public String getWsStatus() {
        WebSocketMessageBrokerStats ws = new WebSocketMessageBrokerStats();
        return ws.toString();
    }
}
