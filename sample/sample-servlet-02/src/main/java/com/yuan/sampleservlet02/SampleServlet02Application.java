package com.yuan.sampleservlet02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class SampleServlet02Application {

    public static void main(String[] args) {
        SpringApplication.run(SampleServlet02Application.class, args);
    }

}
