package com.yuan.samplefilterlistener02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class SampleFilterListener02Application {

    public static void main(String[] args) {
        SpringApplication.run(SampleFilterListener02Application.class, args);
    }

}
