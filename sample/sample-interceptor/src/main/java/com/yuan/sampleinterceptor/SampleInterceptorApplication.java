package com.yuan.sampleinterceptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleInterceptorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleInterceptorApplication.class, args);
    }

}
