上一篇文章已经对定义Servlet 的方法进行了说明，过滤器（Filter）和 监听器（Listener）的注册方法和 Servlet 一样，不清楚的可以查看下上一篇文章：http://blog.csdn.net/catoop/article/details/50501686 本文将直接使用@WebFilter和@WebListener的方式，完成一个Filter 和一个 Listener。

### 通过代码注册过滤器、监听器核心示例代码：
``` 
@Bean
    public FilterRegistrationBean myFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        // 注册自定义过滤器
        filterRegistrationBean.setFilter(new MyFilter());
        // 过滤器名称
        filterRegistrationBean.setName("myFilter");
        // 过滤所有路径
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/*"));
        //优先级，越低越优先，默认值是int最大值
        filterRegistrationBean.setOrder(1);
        return filterRegistrationBean;
    }
    @Bean
    public ServletListenerRegistrationBean mySession() {
        ServletListenerRegistrationBean bean = new ServletListenerRegistrationBean();
        bean.setListener(new MyHttpSessionListener());
        return bean;
    }
    @Bean
    public ServletListenerRegistrationBean myServletContent() {
        ServletListenerRegistrationBean bean = new ServletListenerRegistrationBean();
        bean.setListener(new MyServletContextListener());
        return bean;
    }
```
### 通过注解注册过滤器、监听器核心示例代码：
``` 
1. 在Application类配置注解ServletComponentScan
@ServletComponentScan
public class SpringBootSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSampleApplication.class, args);
    }
}
2. 配置过滤器
@WebFilter(filterName="myFilter",urlPatterns="/*")
public class MyFilter implements Filter {

3. 配置监听器
@WebListener
public class MyHttpSessionListener implements HttpSessionListener {
```