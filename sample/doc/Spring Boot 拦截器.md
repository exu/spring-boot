## 拦截器实现
实现自定义拦截器只需要3步：
1. 创建我们自己的拦截器类并实现 HandlerInterceptor 接口 或 继承HandlerInterceptorAdapter。
2. 实现mvc配置
>   2.1 继承WebMvcConfigurerAdapter，并重写 addInterceptors 方法（springboot2已经废弃WebMvcConfigurerAdapter）。 
>   2.2 实现WebMvcConfigurer接口

配置拦截器代码示例
``` 
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new MyInterceptor1()).addPathPatterns("/**");
        registry.addInterceptor(new MyInterceptor2()).addPathPatterns("/**");
    }
```
## 参考文献
[Spring Boot 拦截器](https://blog.csdn.net/catoop/article/details/50501696)