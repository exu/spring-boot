package com.yuan.samplefilterlistener01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class SampleFilterListener01Application {

    public static void main(String[] args) {
        SpringApplication.run(SampleFilterListener01Application.class, args);
    }

    @Bean
    public FilterRegistrationBean myFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        // 注册自定义过滤器
        filterRegistrationBean.setFilter(new MyFilter());
        // 过滤器名称
        filterRegistrationBean.setName("myFilter");
        // 过滤所有路径
        filterRegistrationBean.setUrlPatterns(Arrays.asList("/*"));
        //优先级，越低越优先，默认值是int最大值
        filterRegistrationBean.setOrder(1);
        return filterRegistrationBean;
    }
    @Bean
    public ServletListenerRegistrationBean mySession() {
        ServletListenerRegistrationBean bean = new ServletListenerRegistrationBean();
        bean.setListener(new MyHttpSessionListener());
        return bean;
    }
    @Bean
    public ServletListenerRegistrationBean myServletContent() {
        ServletListenerRegistrationBean bean = new ServletListenerRegistrationBean();
        bean.setListener(new MyServletContextListener());
        return bean;
    }
}
