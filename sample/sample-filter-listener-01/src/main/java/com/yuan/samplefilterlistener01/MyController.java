package com.yuan.samplefilterlistener01;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class MyController {
    @RequestMapping("/hello")
    public String hello(HttpSession session) {
        return "hello world:"+session.getId()  ;
    }
}
