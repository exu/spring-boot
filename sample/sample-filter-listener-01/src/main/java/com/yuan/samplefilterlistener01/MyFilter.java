package com.yuan.samplefilterlistener01;

import javax.servlet.*;
import java.io.IOException;

/**
 * 通过代码，初始过滤器
 */
public class MyFilter implements Filter {
    @Override
    public void destroy() {
        System.out.println("过滤器销毁");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        System.out.println("执行过滤操作");
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        System.out.println("过滤器初始化");
    }
}
