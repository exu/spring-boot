package com.yuan.sampleservlet02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SampleServlet01Application {
    /**
     * 使用代码注册Servlet（不需要@ServletComponentScan注解）
     *
     * @return
     * @author SHANHY
     * @create  2016年1月6日
     */
    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        // ServletName默认值为首字母小写，即myServlet
        return new ServletRegistrationBean(new MyServlet(), "/xs/*");
    }
    public static void main(String[] args) {
        SpringApplication.run(SampleServlet01Application.class, args);
    }

}
