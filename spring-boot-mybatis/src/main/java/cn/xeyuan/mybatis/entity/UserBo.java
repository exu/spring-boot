package cn.xeyuan.mybatis.entity;

import java.util.Date;

/**
 * 用户实体类
 * @author exu
 * @version 1.0
 * @date 2017/8/11 10:42
 */
public class UserBo implements java.io.Serializable {
    private Integer userId;
    private String userName;
    private Integer userAge;
    private Date createDate;

    public UserBo() {
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
