package cn.xeyuan.mybatis.entity;

import java.util.Date;

/**
 * 描述：BetMatch对象
 * @author exu
 * @version 1.0
 * @since 1.8
 * @date 2016-11-15 16:53:11
 */
public class BetMatch implements java.io.Serializable {

	/**赛事id*/
	private Long bmId;
	/**赛事名称*/
	private String bmName;
	/**赛事logo*/
	private String bmLogo;
	/**游戏appkey*/
	private String bmGameAppid;
	/**赛事开始时间*/
	private Date bmBeginTime;
	/**赛事结束时间*/
	private Date bmEndTime;
	/**赛事创建时间*/
	private Date bmCreateTime;
	/**赛事排序*/
	private Long bmSoft;
	/**是否删除：0否，1是*/
	private Boolean bmIsDelete;
	/**管理后台用户id*/
	private Long bmSuserId;
	//版本号
	private Long bmVersion;
	//赛事简称
//	@NotBlank
//	@Length(min = 1, max = 50)
//	@Des("{betMatch.bmAbbreviation}")
	private String bmAbbreviation;
	//赛事中文名称
//	@NotBlank
//	@Length(min = 1, max = 100)
//	@Des("{betMatch.bmCnName}")
	private String bmCnName;
	//来源:1、手动,2、Abios
	private Short bmSource;
	//赛事abiosId,没有存0
	private String bmAbiosId;
	//abios游戏模块id
	private Long bmAbiosgameId;
	//赛事状态：0未结束,1已结束
	private Short bmStatus;
	//是否编辑：0 false未编辑，1 true已编辑
	private Boolean bmIsEdit;
	//是否添加战队
	private Boolean bmIsAddCorps;
	//columns END

	public BetMatch(){
	}

	public BetMatch(Long bmId){
		this.bmId = bmId;
	}

	public void setBmId(Long value) {
		this.bmId = value;
	}

	public Long getBmId() {
		return this.bmId;
	}
	public void setBmName(String value) {
		this.bmName = value;
	}

	public String getBmName() {
		return this.bmName;
	}
	public void setBmLogo(String value) {
		this.bmLogo = value;
	}

	public String getBmLogo() {
		return this.bmLogo;
	}
	public void setBmBeginTime(Date value) {
		this.bmBeginTime = value;
	}

	public Date getBmBeginTime() {
		return this.bmBeginTime;
	}
	public void setBmCreateTime(Date value) {
		this.bmCreateTime = value;
	}

	public Date getBmCreateTime() {
		return this.bmCreateTime;
	}
	public void setBmSoft(Long value) {
		this.bmSoft = value;
	}

	public Long getBmSoft() {
		return this.bmSoft;
	}
	public void setBmIsDelete(Boolean value) {
		this.bmIsDelete = value;
	}

	public Boolean getBmIsDelete() {
		return this.bmIsDelete;
	}
	public void setBmSuserId(Long value) {
		this.bmSuserId = value;
	}

	public Long getBmSuserId() {
		return this.bmSuserId;
	}

	public String getBmGameAppid() {
		return bmGameAppid;
	}

	public void setBmGameAppid(String bmGameAppid) {
		this.bmGameAppid = bmGameAppid;
	}

	public Date getBmEndTime() {
		return bmEndTime;
	}

	public void setBmEndTime(Date bmEndTime) {
		this.bmEndTime = bmEndTime;
	}

	public Long getBmVersion() {
		return bmVersion;
	}

	public void setBmVersion(Long bmVersion) {
		this.bmVersion = bmVersion;
	}

	public String getBmAbbreviation() {
		return bmAbbreviation;
	}

	public void setBmAbbreviation(String bmAbbreviation) {
		this.bmAbbreviation = bmAbbreviation;
	}

	public String getBmCnName() {
		return bmCnName;
	}

	public void setBmCnName(String bmCnName) {
		this.bmCnName = bmCnName;
	}

	public Short getBmSource() {
		return bmSource;
	}

	public void setBmSource(Short bmSource) {
		this.bmSource = bmSource;
	}

	public String getBmAbiosId() {
		return bmAbiosId;
	}

	public void setBmAbiosId(String bmAbiosId) {
		this.bmAbiosId = bmAbiosId;
	}

	public Long getBmAbiosgameId() {
		return bmAbiosgameId;
	}

	public void setBmAbiosgameId(Long bmAbiosgameId) {
		this.bmAbiosgameId = bmAbiosgameId;
	}

	public Short getBmStatus() {
		return bmStatus;
	}

	public void setBmStatus(Short bmStatus) {
		this.bmStatus = bmStatus;
	}

	public Boolean getBmIsEdit() {
		return bmIsEdit;
	}

	public void setBmIsEdit(Boolean bmIsEdit) {
		this.bmIsEdit = bmIsEdit;
	}

	public Boolean getBmIsAddCorps() {
		return bmIsAddCorps;
	}

	public void setBmIsAddCorps(Boolean bmIsAddCorps) {
		this.bmIsAddCorps = bmIsAddCorps;
	}
}
