package cn.xeyuan.mybatis.mapper;

import cn.xeyuan.mybatis.entity.BetMatch;
import cn.xeyuan.mybatis.entity.UserBo;

import java.util.List;

/**
 * 用户Mapper
 * @author exu
 * @version 1.0
 * @date 2017/8/11 10:41
 */
public interface UserMapper {
  public  List<UserBo> queryAllList();
  public  List<BetMatch> queryMathcTest();

    public   int saveUser(UserBo user);

}
