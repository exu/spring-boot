package cn.xeyuan.mybatis.web;

import cn.xeyuan.mybatis.entity.BetMatch;
import cn.xeyuan.mybatis.entity.UserBo;
import cn.xeyuan.mybatis.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 用户 controller
 *
 * @author exu
 * @version 1.0
 * @date 2017/8/11 10:40
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));//true:允许输入空值，false:不能为空值
    }
    /**
     * 获取用户列表
     *
     * @return
     */
    @RequestMapping("/list")
    public List<UserBo> list() {
        List<UserBo> list = userMapper.queryAllList();
        return list;
    }
    /**
     * 获取用户列表
     *
     * @return
     */
    @RequestMapping("/match")
    public List<BetMatch> match() {
        long startTime = System.currentTimeMillis();
        System.out.println("开始======================"+startTime);
        List<BetMatch> list = userMapper.queryMathcTest();
        long endTime = System.currentTimeMillis();
        long outTime = endTime - startTime;
        System.out.println("执行时间： startTime="+startTime+",endTime="+endTime+"，执行时间="+outTime);
        return list;
    }

    /**
     * 获取用户列表
     *
     * @return
     */
    @RequestMapping("/save")
    public String save(UserBo user) {
        int count = userMapper.saveUser(user);
        return count > 0 ? "保存成功" : "保存失败";
    }
}
