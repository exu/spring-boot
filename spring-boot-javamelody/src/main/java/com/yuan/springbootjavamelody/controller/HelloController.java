package com.yuan.springbootjavamelody.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author 徐恩源
 * @version V1.0
 * @Title
 * @date 2019/10/20 0020 20:58
 */
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        return "hello world";
    }
}
