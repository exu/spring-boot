3.1 JavaMelody 介绍

JavaMelody 是用来在 QA 和实际运行生产环境中监控 Java 或 Java EE 应用程序服务器的一个开源框架。它不是一个工具来模拟来自用户的请求，而是一个测量和计算用户在实际操作中应用程序的使用情况的工具，并以图表的形式显示，图表可以按天，周，月，年或自定义时间段查看。
3.2 集成 JavaMelody

① 首先在项目父 pom 文件中声明 JavaMelody 依赖

<dependency>
    <groupId>net.bull.javamelody</groupId>
    <artifactId>javamelody-spring-boot-starter</artifactId>
    <version>1.74.0</version>
</dependency>

② 其次在 demo-web 层中的 pom 文件中添加上述依赖

<dependencies>
    ...省略其余部分...
    <dependency>
        <groupId>net.bull.javamelody</groupId>
        <artifactId>javamelody-spring-boot-starter</artifactId>
    </dependency>
</dependencies>

③ 最后启动项目，访问 http://localhost:8080/monitoring 查看

④ 为了加强安全性，修改默认访问地址以及设置为登录后才可访问，可在 application.properties 文件中添加以下配置项

javamelody.init-parameters.authorized-users = admin:pwd
javamelody.init-parameters.monitoring-path = /demo/monitoring