package cn.xeyuan.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;


@Component
public class SchedulerTask {
    private int count=0;
    @Scheduled(cron="*/5 * * * * ?")
    private void process1() throws InterruptedException {
        System.out.println(getTime()+"线程111运行次数 "+(count++)+"——线程名称："+Thread.currentThread().getName());
        Thread.sleep(5000);
    }
    private String getTime () {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(new Date());
    }

}
