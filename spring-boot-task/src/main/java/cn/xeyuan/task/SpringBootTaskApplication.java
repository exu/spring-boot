package cn.xeyuan.task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@SpringBootApplication
@EnableScheduling
public class SpringBootTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootTaskApplication.class, args);
	}
	@Bean
	ThreadPoolTaskScheduler threadPoolTaskScheduler() {
		//核心线程数，默认为1
		ThreadPoolTaskScheduler threadPoolTaskExecutor = new ThreadPoolTaskScheduler();
		threadPoolTaskExecutor.setPoolSize(2);
		return threadPoolTaskExecutor;
	}
}
