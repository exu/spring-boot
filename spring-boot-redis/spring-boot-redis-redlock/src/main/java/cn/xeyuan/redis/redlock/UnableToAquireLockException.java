package cn.xeyuan.redis.redlock;

/**
 * Created by Administrator on 2018/2/27.
 * 不能获取锁的异常类
 */
public class UnableToAquireLockException extends RuntimeException {
    public UnableToAquireLockException() {
    }

    public UnableToAquireLockException(String message) {
        super(message);
    }

    public UnableToAquireLockException(String message, Throwable cause) {
        super(message, cause);
    }
}
