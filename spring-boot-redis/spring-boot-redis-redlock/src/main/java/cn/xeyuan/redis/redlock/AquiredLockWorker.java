package cn.xeyuan.redis.redlock;

/**
 * @title 获取锁后需要处理的逻辑
 * @author Administrator
 */
public interface AquiredLockWorker<T> {
    T invokeAfterLockAquire() throws Exception;
}
