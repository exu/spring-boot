package cn.xeyuan.myredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyredisSpringBootStarterTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyredisSpringBootStarterTestApplication.class, args);
	}
}
