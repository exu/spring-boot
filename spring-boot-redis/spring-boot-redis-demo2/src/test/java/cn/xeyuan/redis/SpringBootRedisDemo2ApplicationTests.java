package cn.xeyuan.redis;

import cn.xeyuan.redis.config.RedisConfig;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootRedisDemo2ApplicationTests {
	@Autowired
	private JedisPool jedisPool;
	@Test
	public void contextLoads() throws Exception {
		String value = get("aaa");
		System.out.println("value========="+value);
		Assert.assertEquals(value,"111");
	}
	public String get(String key) throws Exception  {

		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			return jedis.get(key);
		} finally {
			//返还到连接池
			jedis.close();
		}
	}

}
