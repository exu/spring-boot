package cn.xeyuan.redis.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
//configuration等价于xml格式中配置beans
@Configuration
//引用redisconfig配置属性
@EnableConfigurationProperties(RedisConfig.class)
public class RedisConfiguration {
    @Autowired
    private RedisConfig redisConfig;
    //@Bean标注方法等价于XML中配置bean
    @Bean(name= "jedis.pool.config")
    @ConfigurationProperties("spring.redis.pool")
    public JedisPoolConfig jedisPoolConfig () {
        return new JedisPoolConfig();
    }

    @Bean(name = "jedis.pool")
    public JedisPool jedisPool(@Qualifier("jedis.pool.config") JedisPoolConfig config) {
        return new JedisPool(config, redisConfig.getHost(), redisConfig.getPort(), redisConfig.getTimeout(), redisConfig.getPassword());
    }
}
