package cn.xeyuan.redis;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import redis.clients.jedis.JedisPool;

@SpringBootApplication
public class SpringBootRedisDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRedisDemo2Application.class, args);
	}


}
