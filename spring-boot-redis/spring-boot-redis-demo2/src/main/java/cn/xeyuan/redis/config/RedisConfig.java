package cn.xeyuan.redis.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * redis配置数据
 * @author exu
 * @version 1.0
 * @date 2017/8/16 13:26
 */
@ConfigurationProperties(prefix="spring.redis")
public class RedisConfig {
    // Redis数据库索引（默认为0）
    private String database;
    //Redis服务器地址
    private String host;
    //Redis服务器连接端口(默认：6379)
    private Integer port;
    //Redis服务器连接密码（默认为空）
    private String password;
    // 连接超时时间（毫秒）
    private Integer timeout;

    public RedisConfig() {
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
