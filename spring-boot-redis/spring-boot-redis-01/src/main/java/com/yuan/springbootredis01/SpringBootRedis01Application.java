package com.yuan.springbootredis01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRedis01Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootRedis01Application.class, args);
    }

}
