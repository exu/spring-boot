-- redis数量加减，数量最小为0
local num = tonumber(ARGV[2]);
if (redis.call('exists', KEYS[1]) == 1) then
    local isadd = tonumber(ARGV[1]);
    if (isadd >= 1) then
       return redis.call('incrby', KEYS[1], ARGV[2]);
    end;
    if (isadd <= 0) then
         local hot = tonumber(redis.call('get', KEYS[1]));
         if (hot - ARGV[2] >= 0) then
            return redis.call('decrby', KEYS[1], num);
         else
            return redis.call('decrby', KEYS[1], hot);
        end;
    end;
else
     return redis.call('incrby', KEYS[1], num);
end;