-- 自增1
local times = redis.call("incr", KEYS[1])
-- 数值为1的时候设置KEY的超时时间
if times == 1 then
    redis.call('expire',KEYS[1],ARGV[2])
end
-- 判断是否超过设置的最大次数，如果是返回1
if times > tonumber(ARGV[1]) then
    return 1
end
-- 默认返回0表示没有超次数
return 0