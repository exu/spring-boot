package com.yuan.springbootredis01;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.springframework.data.redis.connection.RedisStringCommands.BitOperation;

@SpringBootTest
public class BitMapActiveUserTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * 添加数据
     */
    @Test
    public void addData() {
        // 用户id作为偏移量，设置1号活跃数据
        for (int i = 50; i <= 200; i++) {
            stringRedisTemplate.opsForValue().setBit("active:01", i, true);
        }
        // 设置2号活跃数据
        for (int i = 100; i <= 150; i++) {
            stringRedisTemplate.opsForValue().setBit("active:02", i, true);
        }
        // 合并1号2号活跃数据
        bitOp(BitOperation.AND,"active:0102", "active:01", "active:02");

    }

    /**
     * bitmap实现活跃用户
     */
    @Test
    public void test() {
        System.out.println("查看userId=100,在1号是否活跃:" + stringRedisTemplate.opsForValue().getBit("active:01", 100));
        System.out.println("1号和2号同时活跃用户数量:" + bitCount("active:0102"));
        System.out.println("1号和2号同时活跃用户id:" + getAdvList("active:0102", 200));
        System.out.println("1号和2号同时活跃用户最小用户id:" + bitPos("active:0102"));
    }

    public List<Integer> getAdvList(String cacheKey, int totalCount) {
        int totalNum = totalCount / 63;
        totalNum = totalCount % 63 != 0 ? totalNum + 1 : totalNum;
        List<Integer> userIds = new ArrayList<>();
        // 当前偏移量
        for (int pageNum = 1; pageNum <= totalNum; pageNum++) {
            int offset = (pageNum - 1) * 63;
            // 从偏移量offset开始读取63位无符号整数
            List<Long> bitFieldList = stringRedisTemplate.execute((RedisCallback<List<Long>>) cbk
                    -> cbk.bitField(cacheKey.getBytes(), BitFieldSubCommands.create().get(BitFieldSubCommands.BitFieldType.unsigned(63)).valueAt(offset)));
            // 同时活跃用户
            if (bitFieldList != null && bitFieldList.size() > 0) {
                long valueDec = bitFieldList.get(0) != null ? bitFieldList.get(0) : 0;
//                System.out.println("valueDec=" + valueDec);
                for (int i = 63; i > 0; i--) {
                    if (valueDec >> 1 << 1 != valueDec) {
                        userIds.add(i - 1 + offset);
                    }
                    valueDec >>= 1;
                }
            }
        }
        userIds.sort(Comparator.naturalOrder());
        return userIds;
    }


//    redisTemplate并没有提供直接的方法来调用bitcount，可以通过redisTemplate.execute来执行bitcount方法。
    public long bitCount(String key) {
        return stringRedisTemplate.execute((RedisCallback<Long>) con -> con.bitCount(key.getBytes()));
    }

    public long bitOp(BitOperation bitOp, String destKey, String... keys) {
        byte[][] bytes = new byte[keys.length][];
        for (int i = 0; i < keys.length; i++) {
            bytes[i] = keys[i].getBytes();
        }
        return stringRedisTemplate.execute((RedisCallback<Long>) con -> con.bitOp(bitOp, destKey.getBytes(),
                bytes));
    }
    public long bitPos(String key) {
        return  stringRedisTemplate.execute((RedisCallback<Long>) con -> con.bitPos(key.getBytes(), true));
    }
}
