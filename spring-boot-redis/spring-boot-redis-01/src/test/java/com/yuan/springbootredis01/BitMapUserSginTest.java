package com.yuan.springbootredis01;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class BitMapUserSginTest {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Test
    public void testAddData() {
        int userId = 101;
        setBit(getCacheKey(userId), 0, true);
        setBit(getCacheKey(userId), 3, true);
        setBit(getCacheKey(userId), 4, true);
        setBit(getCacheKey(userId), 6, true);
        setBit(getCacheKey(userId), 7, true);
        setBit(getCacheKey(userId), 8, true);
        setBit(getCacheKey(userId), 9, true);
    }

    /**
     * bitmap实现用户签到示例
     */
    @Test
    public void test() {
        int userId = 101;
        // 偏移量从0开始，所以减1
        LocalDate today = LocalDate.parse("2021-08-10");
        int offset = today.getDayOfMonth() - 1;
        String cacheKey = getCacheKey(userId);
        System.out.println("用户:" + userId + "今天" + (getBit(cacheKey, offset) ? "已经" : "还未") + "签到");
        System.out.println("用户:" + userId + "当前签到了" + bitCount(cacheKey) + "次");
        System.out.println("用户:" + userId + "本月连续签到了" + getContinuousSignCount(cacheKey, today) + "天");
        System.out.println("用户:" + userId + "本月签到情况" + getSignInfo(cacheKey, today));
    }

    private String getCacheKey(int userId) {
//        LocalDate today = LocalDate.now();
        LocalDate today = LocalDate.parse("2021-08-10");
        DateTimeFormatter yyyyMMDft = DateTimeFormatter.ofPattern("yyyyMM");
        String dateMonthIndexStr = today.format(yyyyMMDft);
        String cacheKey = "sign:" + userId + ":" + dateMonthIndexStr;
        return cacheKey;
    }
    /**
     * 设置key字段第offset位bit数值
     * @param key    字段
     * @param offset 偏移量
     * @param value  数值
     */
    public void setBit(String key, long offset, boolean value) {
        redisTemplate.execute((RedisCallback) con -> con.setBit(key.getBytes(), offset, value));
    }
    /**
     * 判断该key字段offset位否为1
     *
     * @param key    字段
     * @param offset 位置
     * @return 结果
     */
    public boolean getBit(String key, long offset) {
        return (boolean) redisTemplate.execute((RedisCallback) con -> con.getBit(key.getBytes(), offset));
    }

    /**
     * 统计key字段value为1的总数
     *
     * @param key 字段
     * @return 总数
     */
    public Long bitCount(String key) {
        return redisTemplate.execute((RedisCallback<Long>) con -> con.bitCount(key.getBytes()));
    }
    /**
     * 统计key字段value为1的总数,从start开始到end结束
     *
     * @param key   字段
     * @param start 起始
     * @param end   结束
     * @return 总数
     */
    public Long bitCount(String key, Long start, Long end) {
        return (Long) redisTemplate.execute((RedisCallback) con -> con.bitCount(key.getBytes(), start, end));
    }
    /**
     * 计算多个key并返回计算后总数
     * @param bitOp 计算类型
     * @param opKey 计算后存储key
     * @param key key
     * @return 总数
     */
    public Long bitOpAndCount(RedisStringCommands.BitOperation bitOp, String opKey, String... key) {
        byte[][] keys = new byte[key.length][];
        for (int i = 0; i < key.length; i++) {
            keys[i] = key[i].getBytes();
        }
        redisTemplate.execute((RedisCallback) con -> con.bitOp(bitOp,opKey.getBytes(), keys));
        return bitCount(opKey);
    }

    /**
     * 获取本月签到信息
     * @param cacheKey
     * @param localDate
     * @return
     */
    public List<String> getSignInfo(String cacheKey, LocalDate localDate) {
        List<String> resultList = new ArrayList<>();
        int dayOfMonth = localDate.getDayOfMonth();
//        System.out.println("dayOfMonth=" + dayOfMonth);
        List<Long> bitFieldList = (List<Long>) redisTemplate.execute((RedisCallback<List<Long>>) cbk
                -> cbk.bitField(cacheKey.getBytes(), BitFieldSubCommands.create().get(BitFieldSubCommands.BitFieldType.unsigned(dayOfMonth)).valueAt(0)));
        if (bitFieldList != null && bitFieldList.size() > 0) {
            long valueDec = bitFieldList.get(0) != null ? bitFieldList.get(0) : 0;
//            System.out.println("valueDec" + valueDec);
            for (int i = dayOfMonth; i > 0; i--) {
                LocalDate tempDayOfMonth = LocalDate.now().withDayOfMonth(i);
                if (valueDec >> 1 << 1 != valueDec) {
                    resultList.add(tempDayOfMonth.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                }
                valueDec >>= 1;
            }
        }
        return resultList;
    }
    /**
     * 获取当月连续签到次数
     * @param cacheKey
     * @param localDate
     * @return
     */
    public long getContinuousSignCount(String cacheKey, LocalDate localDate) {
        long signCount = 0;
        List<Long> list = redisTemplate.opsForValue().bitField(cacheKey, BitFieldSubCommands.create().get(BitFieldSubCommands.BitFieldType
                .unsigned(localDate.getDayOfMonth())).valueAt(0));
        if (list != null && list.size() > 0) {
            long valueDec = list.get(0) != null ? list.get(0) : 0;
//            System.out.println("valueDec..." + valueDec);
            for (int i = 0; i < localDate.getDayOfMonth(); i++) {
                if (valueDec >> 1 << 1 == valueDec) {
                    if (i > 0) {
                        break;
                    }
                } else {
                    signCount += 1;
                }
                valueDec >>= 1;
            }
        }
        return signCount;
    }
    /**
     * 获取当月首次签到日期
     */
    public LocalDate getFirstSignDate(String cacheKey, LocalDate localDate) {
        long bitPosition = (Long) redisTemplate.execute((RedisCallback) cbk -> cbk.bitPos(cacheKey.getBytes(), true));
        return bitPosition < 0 ? null : localDate.withDayOfMonth((int) bitPosition + 1);
    }

}
