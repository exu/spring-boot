package com.yuan.springbootredis01;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class ListTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void test() {
        stringRedisTemplate.opsForValue().set("user:1","张三");
        stringRedisTemplate.opsForValue().set("user:2","李四");
        stringRedisTemplate.opsForValue().set("user:3","王五");
        stringRedisTemplate.delete(Arrays.asList("user:1","user:2","user:3"));
    }
}
