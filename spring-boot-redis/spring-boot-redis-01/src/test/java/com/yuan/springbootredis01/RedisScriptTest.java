package com.yuan.springbootredis01;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.ResourceScriptSource;
import sun.rmi.runtime.Log;

import java.util.Arrays;

/**
 * redis脚本测试用例
 */
@SpringBootTest
public class RedisScriptTest {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void testRedisIncr() {
        System.out.println(stringRedisTemplate.opsForValue().get("aaa"));
        for (int i = 0; i < 12; i++) {
            System.out.println(redisIncr("goodsViews:10",10, 50));
            System.out.println("结果=="+stringRedisTemplate.opsForValue().get("goodsViews:10"));
        }

    }
    /**
     * 高并发计数器
     * @param key 缓存key
     * @param maxCount 最大值
     * @param expire 有效时间（秒）
     * @return 返回1已超过最大值，返回0未超过最大值
     */
    public Long redisIncr(String key, Integer maxCount, Integer expire) {
        Long result = null;
        try {
            //调用lua脚本并执行
            DefaultRedisScript<Long> limitRedisScript = new DefaultRedisScript<>();
            limitRedisScript.setResultType(Long.class);//返回类型是Long
            //redis_incr.lua文件存放在resources目录下的redis文件夹内
            limitRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("redis_incr.lua")));
            result = stringRedisTemplate.execute(limitRedisScript, Arrays.asList(key), String.valueOf(maxCount), String.valueOf(expire));
            System.out.println("==" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    @Test
    public void testredisHot() {
        System.out.println("第1次"+redisHot("hot:01",true,1));
        System.out.println("第2次"+redisHot("hot:01",true,1));
        System.out.println("第3次"+redisHot("hot:01",true,1));
        System.out.println("第4次"+redisHot("hot:01",false,2));
        System.out.println("第5次"+redisHot("hot:01",false,3));
        System.out.println("第6次"+redisHot("hot:01",false,1));
        System.out.println("第7次"+redisHot("hot:01",false,1));
        System.out.println("第8次"+redisHot("hot:01",true,2));
        System.out.println("第8次"+redisHot("hot:01",true,2));
    }
    public Long redisHot(String key, boolean isAdd, int num) {
        DefaultRedisScript<Long> limitRedisScript = new DefaultRedisScript<>();
        limitRedisScript.setResultType(Long.class);//返回类型是Long
        //redis_incr.lua文件存放在resources目录下的redis文件夹内
        limitRedisScript.setScriptSource(new ResourceScriptSource(new ClassPathResource("redis_hot.lua")));
        Long result = stringRedisTemplate.execute(limitRedisScript, Arrays.asList(key), String.valueOf(isAdd?1:0), String.valueOf(num));
        System.out.println("result==="+result);
        return result;
    }
//    https://www.cnblogs.com/merciless/p/13607703.html
}
