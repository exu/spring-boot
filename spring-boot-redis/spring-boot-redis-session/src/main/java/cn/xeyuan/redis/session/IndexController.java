package cn.xeyuan.redis.session;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author exu
 * @version 1.0
 * @date 2017/8/18 14:31
 */
@RestController
public class IndexController {
    @RequestMapping(value = "login")
    public String login(HttpServletRequest request, String username) {

        request.getSession().setAttribute("user", new User(123456, username));
        return "login";
    }

    @RequestMapping(value = "index")
    public User index(HttpServletRequest request, Model model){

        User user = (User) request.getSession().getAttribute("user");

        return user;
    }
}
