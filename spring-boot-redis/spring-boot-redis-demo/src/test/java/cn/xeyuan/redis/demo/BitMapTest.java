package cn.xeyuan.redis.demo;

import org.assertj.core.util.DateUtil;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.connection.RedisStringCommands;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;

import java.nio.file.NotLinkException;
import java.util.Date;
import java.util.List;

@SpringBootTest
public class BitMapTest {
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Test
    public void testAddData() {
        setBit("sign:202108:101",0,true);
        setBit("sign:202108:101",24,true);
        setBit("sign:202108:101",27,true);
        setBit("sign:202108:101",28,true);
        setBit("sign:202108:101",29,true);
    }
    @Test
    public void test() {
        Date date = DateUtil.parse("2021-08-30");
        // 获取日期对于的天数，从0开始，作为偏移量
        int offset = DateUtil.dayOfMonthOf(date) -1;
        System.out.println("offset="+offset);
        String sginKey = "sign:202108:101";
        System.out.println("查看当天是否签到="+getBit(sginKey, offset));
        System.out.println("当月签到次数="+bitCount(sginKey));
        System.out.println("最近连续签到X天="+getKeepSignCount(sginKey, offset));
    }
    /**
     * 设置key字段第offset位bit数值
     * @param key    字段
     * @param offset 偏移量
     * @param value  数值
     */
    public void setBit(String key, long offset, boolean value) {
        redisTemplate.execute((RedisCallback) con -> con.setBit(key.getBytes(), offset, value));
    }
    /**
     * 判断该key字段offset位否为1
     *
     * @param key    字段
     * @param offset 位置
     * @return 结果
     */
    public boolean getBit(String key, long offset) {
        return (boolean) redisTemplate.execute((RedisCallback) con -> con.getBit(key.getBytes(), offset));
    }

    /**
     * 统计key字段value为1的总数
     *
     * @param key 字段
     * @return 总数
     */
    public Long bitCount(String key) {
        return redisTemplate.execute((RedisCallback<Long>) con -> con.bitCount(key.getBytes()));
    }
    /**
     * 统计key字段value为1的总数,从start开始到end结束
     *
     * @param key   字段
     * @param start 起始
     * @param end   结束
     * @return 总数
     */
    public Long bitCount(String key, Long start, Long end) {
        return (Long) redisTemplate.execute((RedisCallback) con -> con.bitCount(key.getBytes(), start, end));
    }
    /**
     * 计算多个key并返回计算后总数
     * @param bitOp 计算类型
     * @param opKey 计算后存储key
     * @param key key
     * @return 总数
     */
    public Long bitOpAndCount(RedisStringCommands.BitOperation bitOp, String opKey, String... key) {
        byte[][] keys = new byte[key.length][];
        for (int i = 0; i < key.length; i++) {
            keys[i] = key[i].getBytes();
        }
        redisTemplate.execute((RedisCallback) con -> con.bitOp(bitOp,opKey.getBytes(), keys));
        return bitCount(opKey);
    }

    /**
     * 连续签到天数
     * @param key
     * @return
     */
    public int getKeepSignCount(String key, int dayOfMonth) {
        // bitfield sgin:202108:101 u31 0
        BitFieldSubCommands bitFieldSubCommands = BitFieldSubCommands.create()
                .get(BitFieldSubCommands.BitFieldType.unsigned(dayOfMonth))
                .valueAt(0);
        List<Long> list = (List<Long>) redisTemplate.execute((RedisCallback) con -> con.bitField(key.getBytes(), bitFieldSubCommands));
        if (null == list || list.isEmpty()) {
            return 0;
        }
        int keepCount = 0;
        long v = list.get(0) == null ? 0 : list.get(0);
        // i表示位移操作次数
        for (int i = dayOfMonth; i > 0; i--) {
            //右移再左移，如果等于自己说明最低位是0，表示未签到
            if (v >> 1 << 1 == v) {
                //低位0且非当天说明连续签到中断了
                if (i != dayOfMonth) {
                    break;
                }
            } else {
                keepCount++;

            }
            //右移一位并重新赋值，相当于把最低位丢弃一位
            v >>= 1;

        }
        return keepCount;
    }
}
