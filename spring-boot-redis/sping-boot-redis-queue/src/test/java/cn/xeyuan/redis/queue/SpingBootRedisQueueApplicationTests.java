package cn.xeyuan.redis.queue;

import net.minidev.json.JSONUtil;
import org.apache.catalina.*;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.comparator.JSONCompareUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.UtilNamespaceHandler;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.RedisListCommands;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.connection.jedis.JedisClusterConnection;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpingBootRedisQueueApplicationTests {

	@Autowired
	StringRedisTemplate template;
	@Autowired
	CountDownLatch latch;
	@Autowired
	private StringRedisConnection stringRedisConnection;
	@Test
	public void testList () {
		User user = new User(1,"张三");
		stringRedisConnection.lPush("kddd",FastJsonUtils.toJSONString(user));
	}
	@Test
	public void testss() throws InterruptedException {
		template.convertAndSend("chat", "Hello from Redis!");
		try {
			Thread.sleep(1000*10);
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.exit(0);
	}

}
