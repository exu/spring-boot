package cn.xeyuan.redis.queue;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.CountDownLatch;

/**
 * 消息接收者
 * @author exu
 * @version 1.0
 * @date 2017/8/17 16:23
 */
public class Receiver {
    @Autowired
    private CountDownLatch latch;


    public Receiver(CountDownLatch latch) {
        this.latch = latch;
    }

    public void receiveMessage(String message) {
        System.out.println("Received <" + message + ">");
        latch.countDown();
    }
}
