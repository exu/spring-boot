package cn.xeyuan.redis.queue;

import java.io.Serializable;

/**
 * @author exu
 * @version 1.0
 * @date 2017/8/17 16:43
 */
public class User implements Serializable {
    private Integer userId;
    private String name;


    public User() {
    }

    public User( String name) {
        this.name = name;
    }
    public User(Integer userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
