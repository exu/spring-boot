package cn.xeyuan.redis.queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author exu
 * @version 1.0
 * @date 2017/8/17 16:43
 */
@RestController
public class UserController {
    @Autowired
    private StringRedisConnection stringRedisConnection;
    String key = "user_apply";

    //报名
    @RequestMapping("/apply/{name}")
    public String apply(@PathVariable("{name}") String name) {
        User user = new User(name);
        stringRedisConnection.lPush(key, FastJsonUtils.toJSONString(user));
        return "已申请报名，请耐心等待";
    }
}
