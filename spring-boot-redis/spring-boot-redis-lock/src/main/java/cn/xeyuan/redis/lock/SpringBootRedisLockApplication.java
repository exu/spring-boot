package cn.xeyuan.redis.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.DefaultStringRedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.StringRedisConnection;

@SpringBootApplication
public class SpringBootRedisLockApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRedisLockApplication.class, args);
	}
	@Bean
	StringRedisConnection stringRedisConnection(RedisConnectionFactory connectionFactory) {
		return new DefaultStringRedisConnection(connectionFactory.getConnection());
	}
}
