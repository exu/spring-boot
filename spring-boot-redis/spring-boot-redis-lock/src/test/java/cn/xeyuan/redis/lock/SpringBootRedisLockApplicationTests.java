package cn.xeyuan.redis.lock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootRedisLockApplicationTests {
	@Autowired
	private RedisLock redisLock;
	String key = "lock.user";
	@Test
	public void contextLoads() throws InterruptedException {
		redisLock.lookDemo(key,5);
	}

	@Test
	public void contextLoads2() throws InterruptedException {
		redisLock.lookDemo(key,5);
	}
}
