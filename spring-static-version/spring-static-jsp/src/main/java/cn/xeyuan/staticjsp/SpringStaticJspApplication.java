package cn.xeyuan.staticjsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
 * SpringBootServletInitializer 相当于整个Web.xml
 */
@SpringBootApplication
public class SpringStaticJspApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringStaticJspApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringStaticJspApplication.class, args);
    }
}
