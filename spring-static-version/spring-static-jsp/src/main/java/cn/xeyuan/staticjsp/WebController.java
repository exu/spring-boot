package cn.xeyuan.staticjsp;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author exu
 * @version 1.0
 * @date 2017/8/9 16:03
 */
@Controller
public class WebController {
    //配置日志

    @RequestMapping(value = {"/","/web"})
    public String index(ModelMap map){
        map.put("title", "HelloWorld");
        System.out.println("進入Controller");
        return "index";
    }
}
