<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="application/javascript" src="<%=basePath%>${urls.getForLookupPath('/js/jquery.min.js')}"></script>
    <style type="text/css" href="<%=basePath%>${urls.getForLookupPath('/css/bootstrap.min.css')}"></style>
    <title>Spring Boot Demo - JSP</title>
</head>


<body>
<h1 id="title">${title}</h1>
<img src="<%=basePath%>${urls.getForLookupPath('/img/abc.jpg')}"/>
</body>
</html>