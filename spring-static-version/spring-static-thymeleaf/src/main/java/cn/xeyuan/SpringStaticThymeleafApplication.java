package cn.xeyuan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringStaticThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringStaticThymeleafApplication.class, args);
	}
}
