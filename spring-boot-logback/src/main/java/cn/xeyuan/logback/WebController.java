package cn.xeyuan.logback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * WebController
 *
 * @author exu
 * @version 1.0
 * @date 2017/8/10 16:03
 */
@RestController
public class WebController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private Logger loggerFile = LoggerFactory.getLogger("webLog");

    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable("name") String name) {
        logger.info("主日志输出 hello {} info级别", name);
        logger.error("主日志输出 hello {} error级别", name);
        loggerFile.info("输出到weblog文件 hello {} info级别", name);
        return "hello" + name;
    }
}
