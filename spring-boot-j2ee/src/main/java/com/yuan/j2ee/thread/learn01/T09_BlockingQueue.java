package com.yuan.j2ee.thread.learn01;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/***
 * 两个线程，一个输出字母，一个输出数字，交替输出1A2B3C4D...26Z。
 * BlockingQueue 阻塞队列(线程安全的)实现.
 * @author 徐恩源
 * @date 2020-01-17 10:14:46
 */
public class T09_BlockingQueue {
    static BlockingQueue<String> q1 = new ArrayBlockingQueue<>(1);
    static BlockingQueue<String> q2 = new ArrayBlockingQueue<>(1);

    public static void main(String[] args) {
        char[] aI = "1234567".toCharArray();
        char[] aC = "ABCDEFG".toCharArray();
        new Thread(() -> {
            // q1设置后，q2未取到值会等待，取到值会继续for循环
            for (char c : aI) {
                System.out.print(c);
                try{
                   q1.put("ok");
                   q2.take();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, "t1").start();

      new Thread(() -> {
            // 取Q1值，若没得则等待，取到后Q1则无值了，设置Q2的值
            for (char c : aC) {
                try {
                    q1.take();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.print(c);
                try {
                    q2.put("ok");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }, "t2").start();

    }

}
