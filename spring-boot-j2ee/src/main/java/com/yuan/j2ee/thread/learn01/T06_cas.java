package com.yuan.j2ee.thread.learn01;

/***
 * 两个线程，一个输出字母，一个输出数字，交替输出1A2B3C4D...26Z。
 * 利用volatile实现,volatile 能保证对单次读/写的原子性却对其他线程是可见的。
 * @author 徐恩源
 * @date 2020-01-17 10:14:46
 */
public class T06_cas {
    enum ReadyToRun{ t1, t2};
    // volatile具备原子操作
    static volatile ReadyToRun ready = ReadyToRun.t1;
    public static void main(String[] args) {
        char[] aI = "1234567".toCharArray();
        char[] aC = "ABCDEFG".toCharArray();
        new Thread(() -> {
            for (char c : aI) {
                // 未获取时，线程会一直等待，并占用CPU
                while (ready != ReadyToRun.t1) {}
                System.out.println(c);
                ready = ReadyToRun.t2;
            }
        }, "t1").start();
         new Thread(() -> {
            for (char c : aC) {
                while (ready != ReadyToRun.t2) {}
                System.out.println(c);
                ready = ReadyToRun.t1;
            }

        }, "t2").start();

    }

}
