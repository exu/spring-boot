package com.yuan.j2ee.thread.learn01;

import java.util.concurrent.atomic.AtomicInteger;

public class T07_AtomicInteger {
    static AtomicInteger threadNo = new AtomicInteger(1);

    public static void main(String[] args) {
        char[] aI = "1234567".toCharArray();
        char[] aC = "ABCDEFG".toCharArray();
        new Thread(() -> {
            for (char c : aI) {
                // 未获取时，线程会一直等待，并占用CPU
                while (threadNo.get() != 1) {}
                System.out.println(c);
                threadNo.set(2);
            }
        }, "t1").start();
        new Thread(() -> {
            for (char c : aC) {
                while (threadNo.get() != 2) {}
                System.out.println(c);
                threadNo.set(1);
            }

        }, "t2").start();
    }
}
