package com.yuan.j2ee.thread.learn01;

import java.util.concurrent.locks.LockSupport;

/***
 * 两个线程，一个输出字母，一个输出数字，交替输出1A2B3C4D...26Z。
 * 利用LockSupport实现
 * @author 徐恩源
 * @date 2020-01-17 10:14:46
 */
public class T08_LockSupport {
    static Thread t1 = null, t2 = null;

    public static void main(String[] args) {
        char[] aI = "1234567".toCharArray();
        char[] aC = "ABCDEFG".toCharArray();
        t1 = new Thread(() -> {
            for (char c : aI) {
                System.out.print(c);
                // 恢复当前线程
                LockSupport.unpark(t2);
                // 无期限暂停当前线程
                LockSupport.park();
            }
        }, "t1");
        t2 = new Thread(() -> {
            // 这里已要先暂停t2，因为t2可能会先要运行
            for (char c : aC) {
                LockSupport.park();
                System.out.print(c);
                LockSupport.unpark(t1);
            }

        }, "t2");
        t1.start();
        t2.start();
    }
}