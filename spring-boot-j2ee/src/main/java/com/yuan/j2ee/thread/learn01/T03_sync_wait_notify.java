package com.yuan.j2ee.thread.learn01;

import java.util.concurrent.CountDownLatch;

/***
 * 两个线程，一个输出字母，一个输出数字，交替输出1A2B3C4D...26Z。
 * 利用 多线程  wait，notify; 利用 volatile关键词或CountDownLatch对象保证线程执行顺序
 * @author 徐恩源
 * @date 2020-01-17 10:14:46
 */
public class T03_sync_wait_notify {

    private static volatile boolean t2Startd = false;

     private static CountDownLatch latch = new CountDownLatch(1);

    public static void main(String[] args) {
        final Object o = new Object();
        char[] aI = "123456789".toCharArray();
        char[] aC = "ABCDEFGHI".toCharArray();

        new Thread(() -> {
//            latch.await();
            synchronized (o) {
                while (!t2Startd) {
                    try {
                        o.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (char c : aI) {
                    System.out.print(c);
                    try {
                        o.notify();
                        o.wait();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                o.notify(); // 必须，否则无法停止程序（因为最后又一个线程处于wait状态）
            }
        }, "t1").start();

        new Thread(() -> {
            synchronized (o) {
                for (char c : aC) {
                    System.out.print(c);
//                    latch.countDown();
                    t2Startd = true;
                    try {
                        o.notify();
                        o.wait();
                    }catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                o.notify();
            }
        },"t2").start();

    }
}
