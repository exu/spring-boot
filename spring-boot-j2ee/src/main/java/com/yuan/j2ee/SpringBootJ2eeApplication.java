package com.yuan.springbootj2ee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJ2eeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJ2eeApplication.class, args);
    }

}
