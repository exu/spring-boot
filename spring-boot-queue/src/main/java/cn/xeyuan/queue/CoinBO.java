package cn.xeyuan.queue;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author exu
 * @version 1.0
 * @date 2017/9/8 9:41
 */
public class CoinBO implements Delayed {
    private long coinId;
    //用户名称
    private String name;
    //状态值
    private int status;
    //到期时间，毫秒
    private long expiretime;

    public CoinBO(String name, int status, long expiretime) {
        this.name = name;
        this.status = status;
        this.expiretime = TimeUnit.MILLISECONDS.convert(expiretime, TimeUnit.SECONDS) + System.currentTimeMillis();
    }

    public long getDelay(TimeUnit unit) {
        return (expiretime - System.currentTimeMillis());
    }

    /**
     * 排序，根据过期时间，最前排放到最先
     * @param o
     * @return
     */
    public int compareTo(Delayed o) {
        if (o == null || !(o instanceof CoinBO)) return 1;
        if (o == this) return 0;
        CoinBO c = (CoinBO) o;
        return c.getExpiretime() > this.getExpiretime() ? 1 : -1;
    }

    public long getExpiretime() {
        return expiretime;
    }

    @Override
    public String toString() {
        return "CoinEn{" +
                "name='" + name + '\'' +
                ", status=" + status +
                ", expiretime=" + expiretime +
                '}';
    }
}
