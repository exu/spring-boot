package cn.xeyuan.queue;

import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author exu
 * @version 1.0
 * @date 2017/9/8 9:44
 */
@Component
public class Task {

    public void checkQueue() {
        CoinBO coinEn = null;
        try {
            coinEn = PushUtil.delayQueue.take();
            System.out.println(PushUtil.dateToString(new Date(),"HH:mm:ss")+"，取出="+coinEn);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
