package cn.xeyuan.queue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.DelayQueue;

/**
 * @author exu
 * @version 1.0
 * @date 2017/9/8 9:44
 */
public class PushUtil {
    public static DelayQueue<CoinBO> delayQueue = new DelayQueue<>();

    public static String dateToString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
}
