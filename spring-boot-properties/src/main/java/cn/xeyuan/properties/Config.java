package cn.xeyuan.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/8/10 0010.
 */
@Configuration
@ConfigurationProperties(prefix="spring.redis")
public class Config {
    private String name;
    private Integer port;
    private List<String> servers = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public void setServers(List<String> servers) {
        this.servers = servers;
    }

    public List<String> getServers() {
        return this.servers;
    }

    @Override
    public String toString() {
        return "Config{" +
                "name='" + name + '\'' +
                ", port=" + port +
                ", servers=" + servers +
                '}';
    }
}
