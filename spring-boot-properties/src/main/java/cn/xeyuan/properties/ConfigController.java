package cn.xeyuan.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/8/11 0011.
 */
@RestController
public class ConfigController {
    @Autowired
    private Config config;
    @Value("${app.description}")
    private String description;

    @RequestMapping("/hello")
    public String hello() {
        StringBuffer sbf = new StringBuffer();
        sbf.append("hello: ").append("description=").append(description)
                .append("  config=").append(config.toString());
        System.out.println("配置文件读取：description=" + description);
        return sbf.toString();
    }
}
